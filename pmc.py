from packages.AlarmTrigger import AlarmTrigger
from packages.EmptyDataFrame import EmptyDataFrame
import pandas as pd
import argparse


def load_input(input):
    """
    Ingest pipe delimited satellite telemetry ASCII text file data into dataframe providing column headers 

    Parameters:
    input (str):  ASCII text file 

    Returns:
    input_df (pandas dataframe):  Dataframe of telemetry data from input file

    """
    
    columns = ['Timestamp','SatelliteId','RedHighLimit','YellowHighLimit','YellowLowLimit','RedLowLimit','RawValue','Component']

    try:
      input_df = pd.read_csv(input, header= None, delimiter="|",names=columns)
      if input_df.empty:
         raise EmptyDataFrame
    except FileNotFoundError:
      exit('File ' + input + ' is not found.  Please try again.')
    except EmptyDataFrame:
      exit('File ' + input + ' is Empty')

    #Dataframe of telemetry data from input file
    return input_df



def send_alert(trig_alarms_df):
    """
    Sends an alert message to screen or console when there are alarm triggers

    Parameters:
    trig_alarms_df (pandas dataframe):  Triggered alarms dataframe in alert message json format 

    Returns:
    
    """
       
    try:
        if trig_alarms_df.empty:
            raise EmptyDataFrame
        print("```javascript")
        print(trig_alarms_df.to_json(orient = "records",  indent=1))
        print("```")
    except EmptyDataFrame:
        exit('No Alarms: trig_alarms_df is Empty.')
    except Exception as e:
        exit(f'OUTPUT ERROR:  {str(e)}')



##############MAIN#################################
"""
   Handles telemetry pipe delimited ASCII file from command line argument.
   Processes component's telemetry data and will send an alert message when there are alarm triggers. 
    
"""

#Command Line Arguments
argparser = argparse.ArgumentParser(description='Paging Mission Control')
#positional argument (required)
argparser.add_argument('input', metavar='ASCII input text file', type=str, help='enter ASCII input text file')
#load input file to dataframe
input_df = load_input(argparser.parse_args().input)

#Component Alarm Trigger Objects
battAlarmTrigObject  = AlarmTrigger(input_df, 'BATT')
tstatAlarmTrigObject = AlarmTrigger(input_df, 'TSTAT')


try:
    #Component Triggered Alarms
    trig_alarms_df_list = [battAlarmTrigObject.trigger_alarm(), tstatAlarmTrigObject.trigger_alarm()]
    #Concatenate component triggered alarms into a single dataframe
    trig_alarms_df = pd.concat(trig_alarms_df_list).reset_index(drop=True)
    
    #Send alert message on triggered alarms
    send_alert(trig_alarms_df)
except TypeError:
    pass
except ValueError:
    #No alarms
    pass
   



