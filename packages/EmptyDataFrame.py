# define Python user-defined exception
class EmptyDataFrame(Exception):
    "Raised when DataFrames are Empty"
    pass
