import pandas as pd
from dateutil import parser
import json


class AlarmTrigger:
    """  
    Trigger telemetry alarms  

    Parameters:
           self (object) :  
           df_input(pandas dataframe) :  ASCII text file of telemetry data
           component(str)  :  telemetry component (battery or tstat)
    """
    def __init__(self,df_input,component):
        #Dataframe of the entire status telemetry data which includes the data records as rows and header columns
        self.df_input = df_input

	#Telemetry component
        self.component = component
      

    def create_time_series(self):
        """
        Identifies time series bins with the first and last component's alarm timestamp range and further splits that into 5 minute interval indexes.
        Creates dataframe of alarms with the 'TimeBin' column containing the alarm's timestamp bin_id.   

        Parameters:
        self (object): 

        Returns:
        alarms_df (pandas dataframe): Alarm dataframe containing the TimeBin column with time bin_id
        """    
        alarms_df = pd.DataFrame()
  
        #Create dataframe that contains rows/alarms where the battery voltage readings are under the red low limit
        if self.component == 'BATT':
            alarms_df = self.df_input.query("Component == @self.component and RawValue < RedLowLimit")
        #Create dataframe that contains rows/alarms where the thermostat readings exceed the red high limit
        elif self.component == 'TSTAT':
            alarms_df = self.df_input.query("Component == @self.component and RawValue > RedHighLimit")
        
	#Alarm condition met                                
        if len(alarms_df.index):
	    #DatetimeIndex which contains the first and last component's alarm timestamp range and further splits that into 5 minute interval indexes (time series bins)
            alarm_time_series_bin = pd.date_range(start=alarms_df['Timestamp'].min(), end=alarms_df['Timestamp'].max(), freq='5min')
            
            #TimeBin list
            tb_list = []
            
            #Identify alarm time bin_id 
            for row in alarms_df.itertuples(index = True): 
                #SatelliteId of alarm   
                sid = getattr(row, 'SatelliteId')     
       
                #Alarm timestamp falls in the LAST bin of the 5 minute interval range
                if pd.Timestamp(getattr(row, 'Timestamp')) >= alarm_time_series_bin[-1]:
                    #Add timebin id to list 
                    tb_list.append(str(sid) + str(len(alarm_time_series_bin)))                                    
                    continue
                #Loop thru DatetimeIndex and identify what bin/index the alarm timestamp is in
                for bin_id,ts in enumerate(alarm_time_series_bin, start=1):    
                    #Alarm timestamp falls in the FIRST bin of the 5 minute interval range                
                    if (pd.Timestamp(getattr(row, 'Timestamp')) >= alarm_time_series_bin[bin_id -1]) and (pd.Timestamp(getattr(row, 'Timestamp')) < alarm_time_series_bin[bin_id]):
                        #Add timebin id to list
                        tb_list.append(str(sid) + str(bin_id))                                            
                        break
                    #Alarm timestamp falls in the current bin of the 5 minute interval range
                    elif (pd.Timestamp(getattr(row, 'Timestamp')) >= alarm_time_series_bin[bin_id]) and (pd.Timestamp(getattr(row, 'Timestamp')) < alarm_time_series_bin[bin_id+1]):
                        #Add timebin id to list
                        tb_list.append(str(sid) + str(bin_id+1))                                            
                        break
            
            #Add new 'TimeBin' column to alarms_df with the alarms(rows) time bin_id
            alarms_df.insert(loc=len(alarms_df.columns),column='TimeBin',value=tb_list)

        #returns dataframe containing alarm time bin_id in the TimeBin column
        return alarms_df
                    

    def trigger_alarm(self):
        """
        Identifies triggered alarms which are three or more of the same satellite alarms withing 5 minute intervals.
        The first satellite triggered alarms in a 5 minute window is formatted to be an alert message.
       
        Parameters:
        self (object): 

        Returns:
        trig_alarm_df (pandas dataframe): Triggered alarms dataframe alert message formatted (satelliteid, severity, component, and timestamp)
        """    
        alarms_df = self.create_time_series()

        #TimeBin list
        tb_list = []
           
        #Alarms
        if len(alarms_df.index) :  
            #Dataframe of triggered alarms identified by three or more of the same satellite alarms within 5 minute intervals   
            trig_alarms_df = alarms_df.groupby(['SatelliteId','Component','TimeBin'])['TimeBin'].size().reset_index(name='Counter').query("Counter >= 3")
            
            #Triggered alarms
            if len(trig_alarms_df.index):
                #Add triggered alarms time bin_ids to a list
                tb_list.append(trig_alarms_df['TimeBin'].tolist())
                
                #Dataframe of the first satellite triggered alarm in 5 minute interval(TimeBin) including SatelliteId, Component, and Timestamp columns
                trig_alarm_df = alarms_df.query("TimeBin == @tb_list[0]").drop_duplicates(subset=['SatelliteId','Component','TimeBin']).loc[:,["SatelliteId","Component","Timestamp"]]

                #Format triggered alarm timestamp                                   
                for row in trig_alarm_df.itertuples(index = True) :          
                    trig_alarm_df.at[getattr(row,'Index'),'Timestamp']=parser.parse(trig_alarm_df.at[getattr(row,'Index'),'Timestamp']).isoformat(timespec='milliseconds') + 'Z'
                
                #Identify component severity   
                if getattr(row,'Component') == 'BATT':
                   severity="RED LOW"
                elif getattr(row,'Component') == 'TSTAT':
                   severity="RED HIGH"

                #Insert Severity column and values to trig_alarm_df in the second position
                trig_alarm_df.insert(1,"Severity",severity)
           
                #Lowercase all trig_alarm_df columns
                trig_alarm_df.columns = trig_alarm_df.columns.str.lower() 

                #return triggered alarms dataframe formatted to be an alert message           
                return trig_alarm_df
            
            
        


